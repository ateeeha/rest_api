<?php

use App\Http\Controllers\PembelianController;
use App\Http\Controllers\PenjualanController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\VendorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/vendor', [VendorController::class, 'index']);
Route::get('/vendor/{kode_vendor}', [VendorController::class, 'show']);
Route::post('/vendor', [VendorController::class, 'create']);
Route::put('/vendor/{kode_vendor}', [VendorController::class, 'update']);
Route::delete('/vendor/{kode_vendor}', [VendorController::class, 'delete']);

Route::get('/produk', [ProdukController::class, 'index']);
Route::get('/produk/{kode_vendor}', [ProdukController::class, 'show']);
Route::post('/produk', [ProdukController::class, 'create']);
Route::put('/produk/{kode_vendor}', [ProdukController::class, 'update']);
Route::delete('/produk/{kode_vendor}', [ProdukController::class, 'delete']);

Route::get('/pembelian', [PembelianController::class, 'index']);
Route::post('/pembelian', [PembelianController::class, 'beli']);

Route::get('/penjualan', [PenjualanController::class, 'index']);
Route::post('/penjualan', [PenjualanController::class, 'jual']);
