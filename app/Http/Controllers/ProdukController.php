<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProdukController extends Controller
{
    public function index()
    {
        return Produk::all();
    }

    public function show($kode_produk)
    {
        return Produk::findOrFail($kode_produk);
    }

    public function create(request $request)
    {
        $v = Validator::make($request->all(), [
            'kode_vendor' => 'required|numeric',
            'nama_produk' => 'required',
        ]);

        if ($v->fails()) {
            return $v->messages();
        }

        $produk = new Produk;
        $produk->kode_vendor = $request->kode_vendor;
        $produk->nama_produk = $request->nama_produk;
        $produk->save();

        return "Data Produk Berhasil Masuk";
    }

    public function update(request $request, $kode_produk)
    {
        $v = Validator::make($request->all(), [
            'kode_vendor' => 'required|numeric',
            'nama_produk' => 'required',
        ]);

        if ($v->fails()) {
            return $v->messages();
        }

        $nama_produk = $request->nama_produk;
        $kode_vendor = $request->kode_vendor;

        $produk = Produk::find($kode_produk);
        $produk->nama_produk = $nama_produk;
        $produk->kode_vendor = $kode_vendor;
        $produk->save();

        return "Data Produk Berhasil di Update";
    }

    public function delete($kode_produk)
    {
        $produk = Produk::findOrFail($kode_produk);
        $produk->delete();

        return "Data Produk Berhasil di Hapus";
    }
}
