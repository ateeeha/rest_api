<?php

namespace App\Http\Controllers;

use App\Models\Penjualan;
use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PenjualanController extends Controller
{
    public function index()
    {
        return Penjualan::all();
    }

    public function jual(request $request)
    {
        $v = Validator::make($request->all(), [
            'kode_produk' => 'required|numeric',
            'jumlah' => 'required|numeric',
        ]);

        if ($v->fails()) {
            return $v->message();
        }

        $produk = Produk::findOrFail($request->kode_produk);
        if ($produk->stok < 1) {
            return "Stok Habis!";
        }

        $produk->stok = $produk->stok - $request->jumlah;
        $produk->save();

        $penjualan = new Penjualan;
        $penjualan->kode_produk = $request->kode_produk;
        $penjualan->jumlah = $request->jumlah;
        $penjualan->save();

        return "Transaksi Penjualan Berhasil di Buat";
    }
}
