<?php

namespace App\Http\Controllers;

use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VendorController extends Controller
{
    public function index()
    {
        return Vendor::all();
    }

    public function show($kode_vendor)
    {
        return Vendor::findOrFail($kode_vendor);
    }

    public function create(request $request)
    {
        $v = Validator::make($request->all(), [
            'nama_vendor' => 'required',
        ]);

        if ($v->fails()) {
            return $v->messages();
        }

        $vendor = new Vendor;
        $vendor->nama_vendor = $request->nama_vendor;
        $vendor->save();

        return "Data Vendor Berhasil Masuk";
    }

    public function update(request $request, $kode_vendor)
    {
        $v = Validator::make($request->all(), [
            'nama_vendor' => 'required',
        ]);

        if ($v->fails()) {
            return $v->messages();
        }

        $nama_vendor = $request->nama_vendor;

        $vendor = Vendor::findOrFail($kode_vendor);
        $vendor->nama_vendor = $nama_vendor;
        $vendor->save();

        return "Data Vendor Berhasil di Update";
    }

    public function delete($kode_vendor)
    {
        $vendor = Vendor::findOrFail($kode_vendor);
        $vendor->delete();

        return "Data Vendor Berhasil di Hapus";
    }
}
