<?php

namespace App\Http\Controllers;

use App\Models\Pembelian;
use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PembelianController extends Controller
{
    public function index()
    {
        dd($request->bearerToken());
        return Pembelian::all();
    }

    public function beli(request $request)
    {
        $v = Validator::make($request->all(), [
            'kode_produk' => 'required|numeric',
            'jumlah' => 'required|numeric',
        ]);

        if ($v->fails()) {
            return $v->message();
        }

        $produk = Produk::findOrFail($request->kode_produk);
        $produk->stok = $produk->stok + $request->jumlah;
        $produk->save();

        $pembelian = new Pembelian;
        $pembelian->kode_produk = $request->kode_produk;
        $pembelian->jumlah = $request->jumlah;
        $pembelian->save();

        return "Transaksi Pembelian Berhasil di Buat";
    }
}
